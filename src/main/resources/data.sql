DROP TABLE IF EXISTS todos;
 
CREATE TABLE todos (
  id LONG AUTO_INCREMENT  PRIMARY KEY,
  user_name VARCHAR(250) NOT NULL,
  description VARCHAR(250) NOT NULL,
  target_date DATE DEFAULT SYSDATE 
);
 
INSERT INTO todos (user_name , description, target_date) VALUES
  ('admin', 'TEsting todos1', SYSDATE ),
  ('admin', 'TEsting todos2', SYSDATE ),
  ('admin', 'TEsting todos3', SYSDATE );

DROP TABLE IF EXISTS ROLE ;
CREATE TABLE ROLE (
  r_id INT NOT NULL PRIMARY KEY,
  r_name varchar(255) DEFAULT NULL
  
);

INSERT INTO ROLE (r_id , r_name) VALUES
  ('1', 'ADMIN'),
  ('2', 'USER'),
  ('3', 'VIEWONLY');
  
DROP TABLE IF EXISTS USER;
CREATE TABLE USER(
userid INT PRIMARY KEY, 
user_name Varchar(255) DEFAULT NULL,
first_name Varchar(255)  NOT NULL,
last_name Varchar(255) ,
email Varchar(255)  NOT NULL,
password varchar(255) DEFAULT NULL,
is_active BOOLEAN DEFAULT TRUE,
created_date DATE,
last_login TIMESTAMP

);

DROP TABLE IF EXISTS USER_ROLE;

CREATE TABLE USER_ROLE (
  user_id INT NOT NULL,
  role_id INT NOT NULL
);

ALTER TABLE USER_ROLE ADD FOREIGN KEY ( user_id ) REFERENCES USER ( userid) ;
ALTER TABLE USER_ROLE ADD FOREIGN KEY ( role_id ) REFERENCES ROLE ( r_id ) ;


DROP TABLE IF EXISTS USER_PROJECTS;
CREATE TABLE USER_PROJECTS(
up_id	INT PRIMARY KEY, 
project_description	Varchar(255)  NOT NULL,
requirement	Varchar(255)  NOT NULL,
hl_estimate	INT NOT NULL,
is_bid_success	BOOLEAN DEFAULT FALSE,
start_date	DATE,
end_date	DATE,
project_active	BOOLEAN DEFAULT TRUE,
created_date	DATE,
updated_date	TIMESTAMP,
requestor INT NOT NULL,
contributor INT DEFAULT NULL

);

ALTER TABLE USER_PROJECTS ADD FOREIGN KEY ( requestor ) REFERENCES USER( userid) ;
ALTER TABLE USER_PROJECTS ADD FOREIGN KEY ( contributor ) REFERENCES USER( userid) ;

DROP TABLE IF EXISTS USER_BIDS;
CREATE TABLE USER_BIDS (
ub_id	INT PRIMARY KEY, 
up_id	INT ,
u_id	INT ,
user_previous_work	Varchar(255),
revised_estimate	INT,
created_date	DATE,
updated_date	TIMESTAMP
);

ALTER TABLE USER_BIDS ADD FOREIGN KEY ( up_id ) REFERENCES USER_PROJECTS ( up_id ) ;
ALTER TABLE USER_BIDS ADD FOREIGN KEY ( u_id ) REFERENCES USER ( userid) ;

DROP TABLE IF EXISTS USER_TODO;
CREATE TABLE USER_TODO(
ut_id	INT PRIMARY KEY, 
up_id	INT,
todo_description	Varchar(255),
end_date	DATE,
is_todo_complete	BOOLEAN,
created_date	DATE  ,
updated_date	TIMESTAMP ,
repository_url	Varchar(255)
);

ALTER TABLE USER_TODO ADD FOREIGN KEY ( up_id) REFERENCES USER_PROJECTS ( up_id) ;