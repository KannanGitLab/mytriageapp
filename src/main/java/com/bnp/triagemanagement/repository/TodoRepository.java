package com.bnp.triagemanagement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bnp.triagemanagement.model.UserTodo;

public interface TodoRepository extends JpaRepository<UserTodo, Integer>{
	List<UserTodo> findByUserName(String user);
}
