package com.bnp.triagemanagement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bnp.triagemanagement.model.UserProject;



@Repository
public interface UserProjectRepository extends JpaRepository<UserProject, Integer> {
	List<UserProject> findByRequestor(int requestor);
}
