package com.bnp.triagemanagement.service;


import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import com.bnp.triagemanagement.model.UserProject;
import com.bnp.triagemanagement.repository.UserProjectRepository;



@Service
public class UserProjectServiceImpl implements UserProjectService {
	@Autowired
    private UserProjectRepository userProjectRepository;
	
    @Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<UserProject> getProjectByUser(int userid) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Optional<UserProject> getProjectById(int projectid) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void updateProject(UserProject userproject) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addTodo(String projectdesc, String projectreq, Date startDate, Date endDate, boolean isDone) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteProject(int projectid) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void disableProject(int projectid) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void saveProject(UserProject upd_userproject) {
		// TODO Auto-generated method stub
		
	}
	


   
}
