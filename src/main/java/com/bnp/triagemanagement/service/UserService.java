package com.bnp.triagemanagement.service;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.bnp.triagemanagement.model.User;
import com.bnp.triagemanagement.dto.UserRegistrationDto;

public interface UserService extends UserDetailsService {

    User findByEmail(String email);

    User save(UserRegistrationDto registration);
}
