package com.bnp.triagemanagement.service;

import java.util.Date;

import java.util.List;
import java.util.Optional;

import com.bnp.triagemanagement.model.UserTodo;

public interface ITodoService {

	List<UserTodo> getTodosByUser(String user);

	Optional<UserTodo> getTodoById(int id);

	void updateTodo(UserTodo todo);

	void addTodo(String name, String desc, Date targetDate, boolean isDone);

	void deleteTodo(int id);
	
	void saveTodo(UserTodo todo);

}