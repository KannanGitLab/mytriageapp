package com.bnp.triagemanagement.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bnp.triagemanagement.model.UserTodo;
import com.bnp.triagemanagement.repository.TodoRepository;

@Service
public class TodoService implements ITodoService {

	@Autowired
	private TodoRepository todoRepository;

	@Override
	public List<UserTodo> getTodosByUser(String user) {
		return todoRepository.findByUserName(user);
	}

	@Override
	public Optional<UserTodo> getTodoById(int id) {
		return todoRepository.findById(id);
	}

	@Override
	public void updateTodo(UserTodo todo) {
		todoRepository.save(todo);
	}

	@Override
	public void addTodo(String name, String desc, Date targetDate, boolean isDone) {
		todoRepository.save(new UserTodo(name, desc, targetDate, isDone));
	}

	@Override
	public void deleteTodo(int id) {
		Optional<UserTodo> todo = todoRepository.findById(id);
		if (todo.isPresent()) {
			todoRepository.delete(todo.get());
		}
	}

	@Override
	public void saveTodo(UserTodo todo) {
		todoRepository.save(todo);
	}
}