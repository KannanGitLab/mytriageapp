package com.bnp.triagemanagement.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.bnp.triagemanagement.model.User;
import com.bnp.triagemanagement.model.UserProject;
import com.bnp.triagemanagement.dto.UserRegistrationDto;

public interface UserProjectService extends UserDetailsService {

	List<UserProject> getProjectByUser(int userid);

	Optional<UserProject> getProjectById(int projectid);

	void updateProject(UserProject userproject);

	void addTodo(String projectdesc, String projectreq, Date startDate,Date endDate, boolean isDone);

	void deleteProject(int projectid);
	
	void disableProject(int projectid);
	
	void saveProject(UserProject upd_userproject);
}
