package com.bnp.triagemanagement.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

@Entity
@Table(name = "user_todo")
public class UserTodo {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int ut_id;
	@Column(name="user_name")
	private String userName;
	
	@Column(name="description")
	@Size(min = 10, message = "Enter at least 10 Characters...")
	private String description;
	
	@Column(name="target_date")
	private Date targetDate;
	
	public UserTodo() {
		super();
	}

	public UserTodo(String user, String desc, Date targetDate, boolean isDone) {
		super();
		this.userName = user;
		this.description = desc;
		this.targetDate = targetDate;
	}

	public int getId() {
		return ut_id;
	}

	public void setId(int id) {
		this.ut_id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getTargetDate() {
		return targetDate;
	}

	public void setTargetDate(Date targetDate) {
		this.targetDate = targetDate;
	}
}