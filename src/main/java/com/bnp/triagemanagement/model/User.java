package com.bnp.triagemanagement.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;



import java.time.LocalDate;
import java.util.Collection;
import java.sql.Timestamp;

@Entity
@Table(name = "user")
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int userid;
	@Column(name="user_name")
	private String userName;
	@Column(name="first_name")
	private String firstName;
	@Column(name="last_name")
	private String lastName;
	@Column(name="email")
	private String email;
	
	@Column(name="is_active")
	private boolean isuserActive;
	
	@Column(name="created_date")
	private LocalDate createdDate;
	@Column(name="last_login")
	private Timestamp lastLogin;
	@Column(name="password")
	 private String password;
	 
	 @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	    @JoinTable(
	            name = "user_role",
	            joinColumns = @JoinColumn(
	                    name = "user_id", referencedColumnName = "userid"),
	            inverseJoinColumns = @JoinColumn(
	                    name = "role_id", referencedColumnName = "r_id"))
	    private Collection<Role> roles;
	 
	

	    public User() {
	    }

	    public User(String firstName, String lastName, String email, String password,boolean isuserActive) {
	    	this.firstName = firstName;
	        this.lastName = lastName;
	        this.email = email;
	        this.password = password;
	        this.isuserActive = true;
	    }

	    public User(String firstName, String lastName, String email, String password,boolean isuserActive, Collection<Role> roles) {
	        this.firstName = firstName;
	        this.lastName = lastName;
	        this.email = email;
	        this.password = password;
	        this.isuserActive = true;
	        this.roles = roles;
	    }
	    
	public int getUserid() {
		return userid;
	}
	public void setUserid(int userid) {
		this.userid = userid;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	public boolean isIsuserActive() {
		return isuserActive;
	}
	public void setIsuserActive(boolean isuserActive) {
		this.isuserActive = isuserActive;
	}
	public LocalDate getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(LocalDate createdDate) {
		this.createdDate = createdDate;
	}
	public Timestamp getLastLogin() {
		return lastLogin;
	}
	public void setLastLogin(Timestamp lastLogin) {
		this.lastLogin = lastLogin;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	public Collection<Role> getRoles() {
		return roles;
	}

	public void setRoles(Collection<Role> roles) {
		this.roles = roles;
	}
	
}
