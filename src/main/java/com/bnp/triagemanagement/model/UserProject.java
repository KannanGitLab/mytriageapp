package com.bnp.triagemanagement.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.time.LocalDate;
import java.util.Collection;
import java.sql.Timestamp;

@Entity
@Table(name = "user_projects")
public class UserProject {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private	int	up_id;
	@Column(name="project_description")
	private	String	projectDescription;
	@Column(name="requirement")
	private	String	requirement;
	@Column(name="hl_estimate")
	private	int	hlEstimate;
	@Column(name="is_bid_success")
	private	boolean	isbidSuccess;
	@Column(name="start_date")
	private	LocalDate	startDate;
	@Column(name="end_date")
	private	LocalDate	endDate;
	@Column(name="project_active")
	private	boolean	projectActive;
	@Column(name="created_date")
	private	LocalDate	createdDate;
	@Column(name="updated_date")
	private	Timestamp	updatedDate;
	@Column(name="requestor")
	private	int	requestor;
	@Column(name="contributor")
	private	int	contributor;
	
	public UserProject() {
		
	}
	public UserProject(int up_id, String projectDescription, String requirement, int hlEstimate, boolean isbidSuccess,
			LocalDate startDate, LocalDate endDate, boolean projectActive, LocalDate createdDate, Timestamp updatedDate,
			int requestor, int contributor) {
		super();
		this.up_id = up_id;
		this.projectDescription = projectDescription;
		this.requirement = requirement;
		this.hlEstimate = hlEstimate;
		this.isbidSuccess = isbidSuccess;
		this.startDate = startDate;
		this.endDate = endDate;
		this.projectActive = projectActive;
		this.createdDate = createdDate;
		this.updatedDate = updatedDate;
		this.requestor = requestor;
		this.contributor = contributor;
	}
	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(
            name = "user",
            joinColumns = @JoinColumn(
                    name = "userid", referencedColumnName = "requestor"))
    private Collection<UserProject> projects;
	
	
	public int getRequestor() {
		return requestor;
	}
	public void setRequestor(int requestor) {
		this.requestor = requestor;
	}
	public int getContributor() {
		return contributor;
	}
	public void setContributor(int contributor) {
		this.contributor = contributor;
	}
	public Collection<UserProject> getProjects() {
		return projects;
	}
	public void setProjects(Collection<UserProject> projects) {
		this.projects = projects;
	}
	public int getUp_id() {
		return up_id;
	}
	public void setUp_id(int up_id) {
		this.up_id = up_id;
	}
	public String getProjectDescription() {
		return projectDescription;
	}
	public void setProjectDescription(String projectDescription) {
		this.projectDescription = projectDescription;
	}
	public String getRequirement() {
		return requirement;
	}
	public void setRequirement(String requirement) {
		this.requirement = requirement;
	}
	public int getHlEstimate() {
		return hlEstimate;
	}
	public void setHlEstimate(int hlEstimate) {
		this.hlEstimate = hlEstimate;
	}
	public boolean isIsbidSuccess() {
		return isbidSuccess;
	}
	public void setIsbidSuccess(boolean isbidSuccess) {
		this.isbidSuccess = isbidSuccess;
	}
	public LocalDate getStartDate() {
		return startDate;
	}
	public void setStartDate(LocalDate startDate) {
		this.startDate = startDate;
	}
	public LocalDate getEndDate() {
		return endDate;
	}
	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}
	public boolean isProjectActive() {
		return projectActive;
	}
	public void setProjectActive(boolean projectActive) {
		this.projectActive = projectActive;
	}
	public LocalDate getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(LocalDate createdDate) {
		this.createdDate = createdDate;
	}
	public Timestamp getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Timestamp updatedDate) {
		this.updatedDate = updatedDate;
	}
	

	
}
