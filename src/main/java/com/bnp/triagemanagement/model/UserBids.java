package com.bnp.triagemanagement.model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "user_bids")
public class UserBids {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private	int	ub_id;
	@Column(name="up_id")
	private	int	usrpeojctId;
	@Column(name="u_id")
	private	int	userId;
	@Column(name="user_previous_work")
	private	String	userpreviousWork;
	public int getUb_id() {
		return ub_id;
	}
	public void setUb_id(int ub_id) {
		this.ub_id = ub_id;
	}
	public int getUsrpeojctId() {
		return usrpeojctId;
	}
	public void setUsrpeojctId(int usrpeojctId) {
		this.usrpeojctId = usrpeojctId;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getUserpreviousWork() {
		return userpreviousWork;
	}
	public void setUserpreviousWork(String userpreviousWork) {
		this.userpreviousWork = userpreviousWork;
	}
	public int getRevisedEstimate() {
		return revisedEstimate;
	}
	public void setRevisedEstimate(int revisedEstimate) {
		this.revisedEstimate = revisedEstimate;
	}
	public LocalDate getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(LocalDate createdDate) {
		this.createdDate = createdDate;
	}
	public LocalDate getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(LocalDate updatedDate) {
		this.updatedDate = updatedDate;
	}
	@Column(name="revised_estimate")
	private	int	revisedEstimate;
	@Column(name="created_date")
	private	LocalDate	createdDate;
	@Column(name="updated_date")
	private	LocalDate	updatedDate;

}
