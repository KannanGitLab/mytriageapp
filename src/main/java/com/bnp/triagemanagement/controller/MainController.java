package com.bnp.triagemanagement.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.ModelAndView;

import com.bnp.triagemanagement.dto.UserRegistrationDto;
import com.bnp.triagemanagement.model.User;
import com.bnp.triagemanagement.service.UserService;

import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class MainController {

    @Autowired
    private UserService userService;
    
	  @GetMapping("/")
	    public String root() {
	        return "index";
	    }

	  @GetMapping("/login")
	    public String login(Model model) {
	        return "login";
	    }
	  
	  @GetMapping("/dashboard")
	    public String dahsboard(Model model) {
	        return "dashboard";
	    }
	  
	  @GetMapping("/todo")
	    public String todo(Model model) {
	        return "todo";
	    }
	  
	  @GetMapping("/createproject")
	    public String createproject(Model model) {
	        return "createproject";
	    }

	  @GetMapping("/bidproject")
	    public String bidproject(Model model) {
	        return "bidproject";
	    }
	  
	    @GetMapping("/user")
	    public String userIndex() {
	        return "user/index";
	    }
	    
	   
}
