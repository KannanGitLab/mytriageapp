package com.bnp.triagemanagement.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.bnp.triagemanagement.model.UserTodo;
import com.bnp.triagemanagement.service.ITodoService;

@Controller
public class DashboardController {
	@Autowired
	private ITodoService todoService;
	
	@RequestMapping(value = "/list-dashboard", method = RequestMethod.GET)
	public String showTodos(ModelMap model) {
		String name = getLoggedInUserName(model);
		model.put("todos", todoService.getTodosByUser(name));
		// model.put("todos", service.retrieveTodos(name));
		return "list-dahsboard";
	}

	private String getLoggedInUserName(ModelMap model) {
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		if (principal instanceof UserDetails) {
			return ((UserDetails) principal).getUsername();
		}

		return principal.toString();
	}

	@RequestMapping(value = "/add-dahsboard", method = RequestMethod.GET)
	public String showAddTodoPage(ModelMap model) {
		model.addAttribute("todo", new UserTodo());
		return "dahsboard";
	}

	@RequestMapping(value = "/delete-dahsboard", method = RequestMethod.GET)
	public String deleteTodo(@RequestParam int id) {
		todoService.deleteTodo(id);
		// service.deleteTodo(id);
		return "redirect:/list-dahsboard";
	}

	@RequestMapping(value = "/update-dahsboard", method = RequestMethod.GET)
	public String showUpdateTodoPage(@RequestParam int id, ModelMap model) {
		UserTodo todo = todoService.getTodoById(id).get();
		model.put("todo", todo);
		return "dahsboard";
	}

	@RequestMapping(value = "/update-dahsboard", method = RequestMethod.POST)
	public String updateTodo(ModelMap model, @Valid UserTodo todo, BindingResult result) {

		if (result.hasErrors()) {
			return "dahsboard";
		}

		todo.setUserName(getLoggedInUserName(model));
		todoService.updateTodo(todo);
		return "redirect:/list-dahsboard";
	}

	@RequestMapping(value = "/add-dahsboard", method = RequestMethod.POST)
	public String addTodo(ModelMap model, @Valid UserTodo todo, BindingResult result) {

		if (result.hasErrors()) {
			return "dahsboard";
		}

		todo.setUserName(getLoggedInUserName(model));
		todoService.saveTodo(todo);
		return "redirect:/list-dahsboard";
	}
}
