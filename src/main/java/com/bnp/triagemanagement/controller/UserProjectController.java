package com.bnp.triagemanagement.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bnp.triagemanagement.model.UserProject;
import com.bnp.triagemanagement.service.UserProjectService;
import com.bnp.triagemanagement.dto.UserProjectDto;

@Controller
@RequestMapping("/projectregistration")
public class UserProjectController {

    @Autowired
    private UserProjectService userprojService;

    @ModelAttribute("userproject")
    public UserProjectDto userRegistrationDto() {
        return new UserProjectDto();
    }

    @GetMapping
    public String showCreateProjectForm(Model model) {
        return "createproject";
    }

    @PostMapping
    public String fetchUserProjects(@ModelAttribute("user") @Valid UserProjectDto userprojectDto,
                                      BindingResult result){

        List<UserProject> existingproject = userprojService.getProjectByUser(userprojectDto.getRequestoruserid());
        if (existingproject != null){
            result.rejectValue("email", null, "There is already an account registered with that email");
        }

        if (result.hasErrors()){
            return "registration";
        }

       
        return "redirect:/projectregistration?success";
    }

}
