package com.bnp.triagemanagement.dto;

import java.time.LocalDate;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

import com.bnp.triagemanagement.constraint.FieldMatch;


public class UserProjectDto {
	@NotEmpty
    private String projectdescription;

    @NotEmpty
    private String requirement;

    @NotEmpty
    private int hlestimate;

    @NotEmpty
    private boolean isbidsuccesssful;
    
    @NotEmpty
    private LocalDate startdate;
    
    @NotEmpty
    private LocalDate enddate;
    
    @NotEmpty
    private boolean isprojectactive;
    
    @NotEmpty
    private int projectid;
    
    @NotEmpty
    private int requestoruserid;
    
    @NotEmpty
    private int contributoruserid;
    
	 public int getProjectid() {
		return projectid;
	}

	public void setProjectid(int projectid) {
		this.projectid = projectid;
	}

	public int getRequestoruserid() {
		return requestoruserid;
	}

	public void setRequestoruserid(int requestoruserid) {
		this.requestoruserid = requestoruserid;
	}

	public int getContributoruserid() {
		return contributoruserid;
	}

	public void setContributoruserid(int contributoruserid) {
		this.contributoruserid = contributoruserid;
	}

	public String getProjectdescription() {
		return projectdescription;
	}

	public void setProjectdescription(String projectdescription) {
		this.projectdescription = projectdescription;
	}

	public String getRequirement() {
		return requirement;
	}

	public void setRequirement(String requirement) {
		this.requirement = requirement;
	}

	public int getHlestimate() {
		return hlestimate;
	}

	public void setHlestimate(int hlestimate) {
		this.hlestimate = hlestimate;
	}

	public boolean isIsbidsuccesssful() {
		return isbidsuccesssful;
	}

	public void setIsbidsuccesssful(boolean isbidsuccesssful) {
		this.isbidsuccesssful = isbidsuccesssful;
	}

	public LocalDate getStartdate() {
		return startdate;
	}

	public void setStartdate(LocalDate startdate) {
		this.startdate = startdate;
	}

	public LocalDate getEnddate() {
		return enddate;
	}

	public void setEnddate(LocalDate enddate) {
		this.enddate = enddate;
	}

	public boolean isIsprojectactive() {
		return isprojectactive;
	}

	public void setIsprojectactive(boolean isprojectactive) {
		this.isprojectactive = isprojectactive;
	}

	

}
